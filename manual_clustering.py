import pandas as pd


df = pd.read_csv('data/accounts_post_processed_20190424.csv')

# trading bot:
#   - do a lot of orders
#   - do mostly orders
#   - do many order cancels
def select_trading_bot(df):
    return (df.limit_order_create_operation > 1000) \
        & ((df.limit_order_create_operation + df.limit_order_cancel_operation \
          + df.call_order_update_operation + df.fill_order_operation) \
              / df.total_ops > 0.9) \
        & ((df.limit_order_cancel_operation / df.limit_order_create_operation) > 0.5)

df["is_trading_bot"] = False
df.loc[select_trading_bot, ['is_trading_bot']] = True


# Witness:
#  - did at least one witness_create
#  - do witness_update_operation (to switch signing key to update backup node)
#  - do asset_publish_feed to pubish MPA prices
def select_witnesses(df):
    return (df.witness_create_operation > 0) \
         & (df.asset_publish_feed_operation > 0) \
         & (df.witness_update_operation > 0)

df["is_witness"] = False
df.loc[select_witnesses, ['is_witness']] = True

# Exchanges / Gateway / Point of sales / Airdrops / ICOs wallets
def select_transfer_hubs(df):
    return ((df.transfer_operation / df.total_ops) > 0.9) \
        & ((df.fill_order_operation / df.total_ops) < 0.1) \
        & (df.transfer_operation > 10000)

df["is_transfer_hub"] = False
df.loc[select_transfer_hubs, ['is_transfer_hub']] = True


df.to_csv(path_or_buf='data/accounts_labelized_20190424.csv', index=False)

# Dex gateway Deposit/Withdrawal adresses:
# - gdex-wallet
# - xbtsx-wallet
# - sparkdex-hot / bitspark-vault
# - rudex-* (rudex-eos, rudex-ppy, ...)
# - openledger-dex

# Cex walets:
# - poloniexwallet
# - bittrex-deposit
# - huobi-pro / huobi-bts-withdrawal
# - bitcoinindonesia 
# - binance-bts-1 / binance-bitshares-gvbdb84k6h / binance-cold-3 / binance-5
# - aex-bts-deposit-wallet
# - hitbtc-exchange
# - idax-deposit
# - gate-io-bts66 / gate-bts-off001
# - zbbts / zbsend / zbsend001 / bitkkchubei002 ? 
# - coinegg-main 

# bots
#  - unemployed 
#  - thmsfnd02 

# witnesses 
#   - clockwork
#   - in.abit
#   - fox
#   - bhuz
#   - xeldal
#   - magicwallet.witness
#   - zapata42-witness
#   - gdex-witness