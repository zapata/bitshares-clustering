\chapter{BitShares blockchain data description}

The BitShares blockchain is a list of block. Each block contains a list of transactions which contains a list of operations.
While this representation is useful to save the history of what happened, it is not usable to retrieve the current state of the blockchain. In fact, to get the current state, all the blocks need to be applied according to the consensus rules.
That is why, to retrieve data from the blockchain, most applications rely on services that keep track of the current state. Those are called API nodes and keep the current state in memory. As the memory is limited and the blockchain is already dozens of gigabytes (>60 Go), some operators offload the state in an ElasticSearch database. This allow ad-hoc queries and statistics.

% include an image of the BitShares architecture.

In this chapter we will describe the available data and it's meaning, in the next one we will explain what and how we extract.

\section{Data available through API nodes}

\subsection{Overview}

The API nodes offers a wide range of data:
\begin{itemize}
    \item block and operation content;
    \item platform configuration parameters;
    \item assets information: characteristics, holders, related price feeds, ...;
    \item account description;
    \item account balances;
    \item market information: order book content, last price, volume, margin positions ...
    \item account operation history;
    \item market trade history;
    \item network information;
    \item vote results;
    \item witnesses, committee members and workers.
\end{itemize}

The API calls offer access to data using specific criteria, mainly by id/names or list of ids/names. This offers poor filtering and aggregation capabilities which should be done on the client side. If such features are needed, the use of ElasticSearch is more appropriate.  

Many API nodes are publicly available and maintained by community members.

\subsection{Account related data}

For the purpose of our project, we will focus on the information related to an account. Following a description of the data available:
 \begin{description}
     \item[Identification:] information that allow the unique identification of the account such as the name and the id; 
     \item[Balances:] the amount of each asset owned;
     \item[Votes:] the proxy account used to delegate the votes, the number of witness/committee members voted, the detail of all the votes;
     \item[Statistics:] aggregated information such as the number of core token owned, the number of operations done, amount of fees paid;
     \item[Authentication:] the public keys or the other account  that have the rights to make operations in behalf of this account \footnotemark;
     \footnotetext{BitShares support dynamic account permission that enable weighted multi-signature accounts, and hierarchical accounts. See \url{https://bitshares.org/technology/dynamic-account-permissions/}.} 
     \item[Authorization:] list of white-listed/blacklisted accounts that can interact with the account;
     \item[Membership:] the referrer account that introduced this account to BitShares, the registrar account who paid the fees to create this users, membership subscription expiration date if user has subscribed to a membership to benefit of reduced fees, the percentage of redistribution of his fees between network/referrer/membership.;
     \item[Cash-back and Vesting:] information of token earned through platform mechanics (referral program, issuers fees, witness or worker pay, ...)
     \item[Pending actions:] pending orders on the market or list of proposed operations that should be signed to be validated.
 \end{description}
 
 All this data could be retrieved using the \texttt{get\_full\_accounts} \footnotemark API call that aggregates all the information related to an account. The call is made using a HTTP JSON-RPC call and the response is in JSON format.  See Appendix \ref{appendix:api:get-full-accounts} for details.

\footnotetext{\url{https://dev.bitshares.works/en/master/api/blockchain_api/database.html\#get-full-accounts}}

\subsection{Account operation history}

Additionally we could retrieve the operation history of an account using \texttt{get\_account\_history} \footnotemark API call. However in order to limit the memory consumption most API nodes limit the history size to a small number (usually 100), so the information is only partial.

\footnotetext{\url{https://dev.bitshares.works/en/master/api/blockchain_api/history.html\#get-account-history}}

We could not use the block and operation API neither as some operations are not explicitly stored in the block chain because they are the result of the consensus rule. An example is when a market order is filled, the operation is not explicitly made by an account but a result of the matching of two orders. Those operations are called \texttt{virtual operations}.

We will see below that the ElasticSearch plugin solves the issue.

BitShares allow an extensive list of different operations that represents all the capabilities of the platform. The exhaustive list could be found at Appendix \ref{appendix:api:operation-types}. Each operation type contains his own set of data that describe the computed action. As an example a transfer of asset operation will have at least the origin account, the destination account, the asset id, and the amount. Whereas a limit order operation will have the amount to sell, the asset id to sell, the minimum to receive and an expiration date.

\section{Data available in ElasticSearch}

Thanks to some plugins in the core BitShares services, most of the data could be exported to an ElasticSearch database. This allows to remove the memory constraints, and gives more flexibility in the search criteria and aggregations thanks to the extensive ElasticSearch querying capabilities.

The available indexes in ElasticSearch are:
\begin{itemize}
    \item \textbf{bitshares-*}: all the operations, including the virtual operations;
    \item \textbf{object-accounts}: account characteristics;
    \item \textbf{object-balance}: the user's asset balances;
    \item \textbf{object-assets}: asset descriptions;
    \item \textbf{object-bitassets}: dynamic characteristics of market pegged assets, such as price feeds;
    \item \textbf{object-proposal}: proposed transactions.
\end{itemize}

Depending on how the plugins are configured, the \texttt{object-*} indexes may contain only the last version of every object, or all the subsequent updates of the object.

Multiple ElasticSearch databases are provided by the community members and publicly available in read only. They also offer a Kibana \footnotemark service on top of it to easily query, discover and visualize the information.

\footnotetext{\url{https://www.elastic.co/products/kibana}} 

In our case we will be mainly interested in the \texttt{bitshares-*} index that contains the operations as this is the data missing from the API nodes. The rest of the information could be retrieved from the API nodes. You will find in Appendix \ref{appendix:es:operation} an example of a call to get an operation and it result. ElasticSearch will allow use to query all the operations of a certain user or a certain type, and do some aggregations like count the number of results.

We will see in the next chapter how the data is extracted.