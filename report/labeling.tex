\chapter{Unsupervised clustering}

From the exploration of the data we can see that there is orthogonal uses of the platform that gives us hope to be able to profile the users.
We will see in this chapter if we can identify some profiles using unsupervised classification and human expertise correction.
The most used algorithm to cluster the data is K-means, which we will use. We will see how we prepare the data to improve the results, how we can evaluate the result, and finally analyse the results.
The \texttt{scikit-learn} Python library will be used for the implementation.

\section{Data normalization}

In the dataset not all variables have the same unit. \textbf{auths\_owner\_count} is a number of signature with a range from 1 to 10, while \textbf{total\_ops} is a the number of operations whith a range from 0 to $10^8$. In order to be able to compare the distances of all those axes is better to standardize the data.

K-means is sensitive to the variance of the features in order to avoid to put more weight to some features. In our dataset we have very different variances. \textbf{witness\_create\_operation} is used 0 or 1 time, while the number of BTS owned (\textbf{core\_in\_bts}) differs a lot between the users. We should normalize the features.

Also as we have seen in the exploration phase we have lot of outliers in our features: we had to use logarithmic scale to display them. Few accounts make lot of operations or own lot of BTS. Different scalers implementations have very different behaviours on the outliers as documented in \texttt{scikit-learn} library \footnotemark.

\footnotetext{ \url{https://scikit-learn.org/stable/auto\_examples/preprocessing/plot\_all\_scaling.html}}

In order to choose the appropriate scaling and understand them, they have all been tested and visually checked using figures as below. Additionally to all preimplemented \texttt{scikit-learn} preprocessing classes a custom scaler with logarithmic transformation then standard scaling have been tested. See \texttt{standardization.ipynb} Jupyter notebook for the code. Below the result of the unscaled data, the power transformer and the custom pipeline, with on the left the full data visualization of \textbf{transfer\_operation} and \textbf{fill\_order\_operation} features, and on the right the zoomed dataset on the 0 to 99th percentile.

\begin{listing}[H]
\begin{codebox}{python}
pipe = Pipeline([('log', FunctionTransformer(func=np.log1p, \
                                             inverse_func=np.expm1, \
                                             validate=True) ),
                 ('scale', StandardScaler() )])
\end{codebox}
\caption{Custom pipeline - logarithmic transformation + standard scaler}
\end{listing}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=.40]{clustering/scaling_unscaled.png}
\includegraphics[scale=.40]{clustering/scaling_power.png}
\includegraphics[scale=.40]{clustering/scaling_log.png}
\caption{Data normalization experiments}
\end{center}
\label{clustering:figure:normalization}
\end{figure}

The transformations that seems to offer the best result is the power transformation  with Yeo-Johnson method (Box-Cox could not be used as we have zeros so non strictly positive data), and the custom pipeline using logarithmic transformation followed by standard scaling. They scale all the features to the same range, reduce the variance, while preserving the outliers in a reasonable range.

The normalization is also usefull for features decomposition algorithms as we will see next.
 
\section{Feature reduction}

We have more than 80 features, so to have better results we might want to try with less features.
For that purpose we can use Principal Component Analysis algorithm. Below the cumulative impact of each components. 

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=.60]{clustering/pca_decomposition.png}
\caption{PCA cumulative impact of components}
\end{center}
\label{clustering:figure:pca}
\end{figure}

 The PCA requires normalized data. We will use the method explained in the previous section to prepare the data.
 
 Our tests wil be done with 3 types of feature reduction: none, a PCA with 30 components retained (approximately 80\% of variance), and a PCA with 20 components retained (approximately 60\% of variance).
 
 
\section{Choice of number of clusters}

In order to perform the K-means algorithm, a number of clusters should be provided. There is no rule to find the right number.

As our dataset have many features and many points there is no easy way to interpret the result and know if the clustering is appropriate.

As a first approach a manual investigation process has been used. Multiple number of clusters have been experimented. Then the results have been explored using manual queries on the dataframe, the means values of clusters, and the size of each cluster. While it is possible to identify some clusters, the process is time consuming and cannot scale for multiple experiments and pipeline iterations.

A second approach is to use techniques that reduce the number of feature to be able to visualize the dataset. The most used algorithm is t-SNE. Unfortunately with our dataset of hundred of thousands of accounts and more than 80 features the computation time is very long (I have interrupted all my attempts after several hours of computation). Even if it may be possible to get a result for the dataset, the usability will be low as this method will require too much processing power for many experiments.

Another approach to get a rough idea of how much cluster to use when we don't have labeled data, is to use metrics based on the model itself. One of them is the silhouette coefficient, a measure of how close each point in one cluster is to points in the neighboring clusters. This measure has a range of [-1, 1]. Silhouette coefficients (as these values are referred to as) near +1 indicate that the sample is far away from the neighboring clusters. A value of 0 indicates that the sample is on or very close to the decision boundary between two neighboring clusters and negative values indicate that those samples might have been assigned to the wrong cluster. A silhouette analysis has been performed with 2 to 10 clusters. The code can be found in \texttt{silhouette\_analysis.ipynb} Python notebook. For all the number of clusters we had a silhouette average score between 0.43 and 0.44. 
Below the results for 4, 8, 10 clusters with on the left the silhouette result plot and on the right the visualization of the 2 first components of the PCA with different color for each cluster as a rough visualization.

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=.30]{clustering/silhouette_4.png}
\includegraphics[scale=.30]{clustering/silhouette_8.png}
\includegraphics[scale=.30]{clustering/silhouette_10.png}
\caption{Silhouette analysis}
\end{center}
\label{clustering:figure:silhouette}
\end{figure}


For every number of clusters, we have a big cluster and various small ones with some miss categorized accounts (a negative silhouette score). With higher number of cluster (8 to 10) there are some small clustered that looks well categorized. As we don't expect to have all accounts classified correctly, as many of them could have multiple profiles, we will use high number of clusters so we can have a chance to identify precisely some group of accounts. In our experiments we will use 4 (as a benchmark), 8 and 10 clusters.

\section{Manual labelling} \label{clustering:section:manual-labelling}

It would be nice to have a metric to estimate the quality of our clusters with more accuracy.
Most of the metrics needs labeled data to compute the accuracy of the clusters. We don't have such data, but we can find some business rules to tag the accounts, this could give us hints on the account profiles.

We will start with three tags: trading bots, witnesses and transfer hubs.

\pagebreak

\subsection{Witnesses}

An active witness is a validator of the network, it signs new blocks. His operation footprint is:
\begin{itemize}
    \item at least one \textbf{witness\_create\_operation};
    \item uses \textbf{witness\_update\_operation} (to switch signing key to update backup node);
    \item uses \textbf{asset\_publish\_feed} to pubish Market Pegged Assets prices.
\end{itemize}

There are other ways to get the list of the witnesses using the blockchain current state, but we are interested here in finding the profile from the available features (mainly the operation counts). Also this should give us the list of the witnesses that have been active at some point in time (an information that could not be easily found otherwise). The code used to label the accounts is as below.

\begin{listing}[H]
\begin{codebox}{python}
def select_witnesses(df):
    return (df.witness_create_operation > 0) \
         & (df.asset_publish_feed_operation > 0) \
         & (df.witness_update_operation > 0)

df["is_witness"] = False
df.loc[select_witnesses, ['is_witness']] = True
\end{codebox}
\caption{Labeling through business rules - witnesses}
\end{listing}

\subsection{Transfer hubs}

A transfer hub is an account used to transfer assets from the BitShares blockchain to somewhere else through an off chain application. This regroup Exchanges, Gateways, Tellers, Point of Sales profiles, Airdrop accounts. It characteristics are:

\begin{itemize}
    \item Most of the operations done are transfers;
    \item Should not trade, so very low filled orders;
    \item Should have done many transfers.
\end{itemize}

The below code is used to label this type of accounts. The constants have been chosen empirically.

\begin{listing}[H]
\begin{codebox}{python}
def select_transfer_hubs(df):
    return ((df.transfer_operation / df.total_ops) > 0.9) \
        & ((df.fill_order_operation / df.total_ops) < 0.1) \
        & (df.transfer_operation > 10000)

df["is_transfer_hub"] = False
df.loc[select_transfer_hubs, ['is_transfer_hub']] = True
\end{codebox}
\caption{Labeling through business rules - transfer hubs}
\end{listing}

This labels regroup multiple profiles, but with the current features, it is hard to get more precise labels.

\subsection{Trading bots}

Trading bots are automated traders. Their behaviour is:

\begin{itemize}
    \item do a lot of orders;
    \item do mostly orders;
    \item do many order cancels.
\end{itemize}

\pagebreak

Below the code used to label them. The constants have been chosen empirically.

\begin{listing}[H]
\begin{codebox}{python}
def select_trading_bot(df):
    return (df.limit_order_create_operation > 1000) \
        & ((df.limit_order_create_operation + df.limit_order_cancel_operation \
          + df.call_order_update_operation + df.fill_order_operation) \
              / df.total_ops > 0.9) \
        & ((df.limit_order_cancel_operation / df.limit_order_create_operation) > 0.5)

df["is_trading_bot"] = False
df.loc[select_trading_bot, ['is_trading_bot']] = True
\end{codebox}
\caption{Labeling through business rules - trading bots}
\end{listing}

\subsection{Result}

All the results are saved in a new dataset with 3 new columns \textbf{is\_witness}, \textbf{is\_transfer\_hub} and \textbf{is\_trading\_bot}. We got 61 transfer hubs, 49 witnesses, 3518 trading bots. No accounts have more than one label.
With a human review of the labels we can say that those clusters are quite accurate. They are provided as an hint to evaluate the clustering algorithm, not to be the ground truth.

\section{Pipeline optimization}

To summarize we will experiment clustering with the below parameters.

\begin{table}[!ht]
    \begin{center}
    \begin{tabular}{|l|c|c|c|c|}
    \hline
    Parameter & Values \\
    \hline
    Standardization method & Standard scaler, Power (Yeo-Johnson), Custom log + standard scaler \\
    PCA & none, PCA with 30 components, PCA with 20 components \\
    Number of clusters & 4, 8, 10 \\
    \hline
    \end{tabular}
    \caption{K-means experimentation parameters}
    \end{center}
    \label{clustering:table:experimentation-parameters}
\end{table}

To be able to evaluate the results we generate the following information using our manually labelled data:

\begin{itemize}
    \item Tag inclusion matrix: For each cluster and each tag, how much of this tag is included in the cluster;
    \item Cluster accuracy matrix; for each cluster and each tag, how much of the cluster is tagged with the tag;
    \item Size of each cluster.
\end{itemize}

The first two metrics are inspired by the homogeneity and the completeness that are desirable objectives of a clustering algorithm. But as we have only partially labeled data, and less clusters than targeted, we can't use the pre implemented \texttt{scikit-learn} 
methods.

\pagebreak

The code below is used to generate the scores.

\begin{listing}[H]
\begin{codebox}{python}
def compute_score(df):
    aggs = OrderedDict([
        ('id', 'count'),
        ('is_trading_bot', 'sum'),
        ('is_witness', 'sum'),
        ('is_transfer_hub', 'sum')])

    stats = df.groupby('cluster').agg(aggs).rename(columns={"id": "count"})  
    tag_matching = stats[['is_trading_bot', 'is_witness', 'is_transfer_hub']] \
            .apply(lambda x: (x / x.sum()))
    cluster_fill = stats[['is_trading_bot', 'is_witness', 'is_transfer_hub']] \
            .apply(lambda x: (x / stats['count']))
    return (tag_matching.max(axis=1) > 0.5).sum() + (cluster_fill.max(axis=1) > 0.5).sum()
\end{codebox}
\caption{Computation of cluster evaluation metrics}
\end{listing}

Below an example of the visualization of the result for a human evaluation.

\begin{figure}[!ht]
\begin{center}
\includegraphics[scale=.50]{clustering/metrics_visualization.png}
\caption{Cluster evaluation metrics visualization}
\end{center}
\label{clustering:figure:metrics}
\end{figure}

\section{Result}

Every combination of parameters have run twice. For each the score and the visual information have been analyzed. We worked only on the real account subset to reduce the size of the dataset and get faster results. Our findings are:

\begin{itemize}
    \item Without standardization results are really bad (one group with almost everyone, and small groups of few (up to 30) individuals. Power (Yeo-Johnson) and Custom log + standard scaler obtain similar results. However log + standard scaler is much faster to process.
    \item Feature reduction has no significant impact on the result neither in term of processing time, nor in result quality.
    \item As expected following silhouette analysis the more cluster we set, the better the results are, so the best choice is 10.
    \item With 8 or 10 clusters, with always get a cluster that contains almost all the witnesses (90+\%) and almost only that (85+\%).
    \item No combination allow to classify trading bots and transfer hubs as we did with our business rules, further analysis is needed to interpret each group.
\end{itemize}

\pagebreak

As an example below the result of the best set of parameters (log + stadard scaler, no pca, 10 clusters).

\begin{figure}[h]
\begin{center}
\includegraphics[scale=.60]{clustering/best_result_score.png}
\caption{Evaluation metrics of the best clustering}
\end{center}
\label{clustering:figure:best_result_score}
\end{figure}

From the manual analysis of the results we can say that:
\begin{itemize}
    \item As expected cluster 6 are the witnesses. They do many publications of asset feeds, use create witness operation, update witness operations, and almost no transfer operations. Almost all accounts tagged as witnesses by our business rules are in this cluster. Those not tagged are non active witness as they don't use the update witness operation that is used when updating the producing nodes. Witness accounts are rarely used to do something else, so they have been clearly segregated.
    \item Users that use the withdraw feature have been grouped in cluster 8. Withdraw is an advanced functionality almost not used. We can find some users of this functionality in other clusters and a witness is in this cluster. As our users might have multiple different behaviours this is expected. Selecting only one cluster for every user might be inappropriate, we might want to have multiple tags on every account.
    \item Cluster 7 regroup most of the active voters. They vote for many items, they voted recently, and they have a good amount of followers that delegate them their vote. We can still find some voters in other clusters, but this one could represent the average active user who has no more specific behaviour.
    \item Cluster 0 represents the accounts almost not used or receptacle as the cold wallets. They have few operations.
    \item Cluster 1 are the asset issuers. It includes the gateways. They create and manage assets and use all the related features. They also create a lot of accounts and have many followers as they are well known entities.
    \item Cluster 9 has a high density of users with many different assets.
    \item Cluster 2, 3, 4, 5 are harder to identify, they seems to represent different variations of traders. 
    \item In general the boundaries between clusters are not very clear (as the silhouette graph showed us). Most probably the current features do not allow us to be more precise. Also many accounts have multiple profiles, so classify them in only one group is inaccurate.
\end{itemize}

We can the visualize all the clusters means in radar graphs.

\begin{figure}[!ht]
\begin{center}
\includegraphics[scale=.50]{clustering/best_result_means.png}
\caption{Clusters means visualization}
\end{center}
\label{clustering:figure:best_result_means}
\end{figure}

     
