\chapter{Supervised Classification}

In this chapter we will try to use our data labelled using business rules as seen in Manual Labeling section \ref{clustering:section:manual-labelling} to train supervised models in order to predict the class of each account. We will first have general considerations on our problem, see what scoring function to use, and finally analyze the learning behaviour of decision tree based models.

\section{Target class labels characteristics}

Our goal here is to try to provide a class to every account. We have generated 3 tags: \textbf{is\_trading\_bot}, \textbf{is\_transfer\_hub} and \textbf{is\_witness}. Each account is only in one of those classes. This means we are in a multiclass problem, with 4 classes. The fourth class is a "default" one, as it's an account that have not been labelled.

We know that the labels have been generated using business rules, they might be wrong, or partially complete. We will take this into account in the choice of our model and in the evaluation of the result.

To extract our reference labels ($y$) we use the following code:

\begin{listing}[H]
\begin{codebox}{python}
def compute_label(row):
    if row.is_witness:
        return 1
    if row.is_trading_bot:
        return 2
    if row.is_transfer_hub:
        return 3
    return 0

y = df.apply (lambda row: compute_label(row), axis=1)
\end{codebox}
\caption{Target class labels extraction}
\end{listing}

\pagebreak

We will work only on the real account subset. The distribution of every class is:
\begin{table}[!h]
    \begin{center}
    \begin{tabular}{|l|c|}
    \hline
    Class & Number of accounts \\
    \hline
    Witness & 49 \\
    Transfer hub & 61 \\
    Trading Bot & 3518 \\
    Default & 243140 \\
    Total & 246729 \\
    \hline
    \end{tabular}
    \caption{Classes distribution}
    \end{center}
    \label{predict:table:classes-distribution}
\end{table}

So we are in the case of unbalanced classes. Also we are not interested in the classification of the account in the default class, as this is like it has not been classified. We are interested in accounts that are classified in one of the other classes. We should take care of those considerations to build our scoring function.

\section{Scoring function}

In order to evaluate the quality of our predictions we should set a scoring method. The default one is the accuracy that computes the predicted labels that exactly matches the target ones, which is a really poor metric in our case as our default class represent most of the accounts. As an example if we use the \texttt{DummyClassifier} that always return the highest class (the default one), we get an accuracy of $0.97$.

The first thing we can do it's to ignore the default class when we compute the score. So if we retry the precedent test, we get an accuracy of $0.00$ as none of our labeled account has been found.

As we know that our business classification is not exhaustive, we want to be permissive with false positives. The recall metric is the best choice as it allows more permissive classes than the one we computed.
However our classes are small and, as we ignore the default class, our model can easely build classes too big to get a high recall. So in order to avoid to loose the precision information we will use the fbeta score with a higher weight on the recall.

F1 and Fbeta functions compute the score for one class, in case of multi classes we need to define an average in order to know how to combine the score of each class. We have unbalanced classes (even if the default class is removed) but we decide to give them equal importance in the metric so we use the 'micro' average methodology. Rather than summing the metric per class, this sums the dividends and divisors that make up the per-class metrics to calculate an overall quotient. This is well suited when a majority class is ignored as in our case with the default class.

This means the resulting score function will be:

\begin{listing}[H]
\begin{codebox}{python}
from sklearn.metrics import fbeta_score, make_scorer
scoring = make_scorer(fbeta_score, beta=2, average='micro', labels=[1, 2, 3])
\end{codebox}
\caption{Scoring function}
\end{listing}

\pagebreak

\section{Test set construction}

In order to validate our results, some of the data will be kept for a final evaluation. As we have unbalanced classes we need to make sure we have a stratified split: a split that maintains the ratio between classes. Then in order to find the ratio between test and train data amount, we will use a learning curve analysis.

For the learning curve analysis we do a cross validation between multiple train/test ratio. To generate our various training sets we will use the \texttt{StratifiedKFold} generator, with 4 folds (so 75\% training 25\% test) on a shuffled dataset. 

\begin{listing}[H]
\begin{codebox}{python}
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, stratify=y)

from sklearn.model_selection import StratifiedKFold
cv = StratifiedKFold(n_splits=4, shuffle=True)
\end{codebox}
\caption{Dataset splitting strategies}
\end{listing}

The resulting learning curve is as below. As with have a huge dataset, we don't really care about the ratio until we have at least 75000 samples for the training (30\% of the whole dataset) to avoid over-fitting.

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=.50]{predict/learning_curve_decision_tree.png}
\caption{Learning curve}
\end{center}
\label{predict:figure:learning_curve}
\end{figure}

\pagebreak

\section{Model selection and parameter tuning}

To make our prediction we will use a \texttt{DecisionTreeClassifier} as it's based on binary conditions on features to split the dataset. The same kind of approach we used with our business rules. We expected to get good results with this kind of approach. Another advantage of those models is that they do not need any strandardisation. We can take the dataset as is. However we should take a special care to not overfit or underfit, so we will analyze the validation curves of the main meta parameters.

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=.50]{predict/validation_curve_decision_tree_features.png}
\includegraphics[scale=.50]{predict/validation_curve_decision_tree_max_depth.png}
\includegraphics[scale=.50]{predict/validation_curve_decision_tree_min_samples_leaf.png}
\includegraphics[scale=.50]{predict/validation_curve_decision_tree_min_samples_split.png}
\caption{Meta parameters validation curves}
\end{center}
\label{predict:figure:validation_curves}
\end{figure}

From the validation curves we can see that:
\begin{itemize}
    \item \texttt{max\_depth} underfit when it's too small and then overfit. 10 is just enough so it's the best value.
    \item \texttt{max\_samples\_leaf} and \texttt{min\_sample\_split} overfit when they are at the minimum but underfit otherwise. Also the variability seriously increase when the values increase. This is because the decision tree is not able to cut precisely enough to build small classes such as  witness and transfer hub classes.
    \item \texttt{max\_features} underfit when below 30. The result is very variable as it strongly depends on what features are chosen. The best result is obtained with all the features.
\end{itemize}

Using those findings we can compute a grid search cross validation to find the best parameters (highligted in blod in the below table).

\begin{table}[ht]
    \begin{center}
    \begin{tabular}{|l|c|}
    \hline
    Parameter & Values \\
    \hline
    max\_depth & None, \textbf{10} \\
    criterion & gini, \textbf{entropy} \\
    min\_samples\_split & \textbf{2}, 10, 50 \\
    min\_samples\_leaf & \textbf{1}, 10, 30 \\
    \hline
    \end{tabular}
    \caption{Meta parameters optimization}
    \end{center}
    \label{predict:table:meta_parameters_optimization}
\end{table}

\pagebreak

\section{Result}

With our best parameters \texttt{max\_depth = 10}, \texttt{criterion = entropy}, \texttt{min\_sample\_split = 2} and \texttt{ min\_samples\_leaf = 1} we achieve a cross validated result of \textbf{0.939} with a standard deviation of $0.01$.

Below the confusion matrix where we can see that, if we ignore the default category, we have very few errors of classification. Only one trading bot classified as a witness and one trading bot as a transfer hub. 

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=.60]{predict/confusion_matrix.png}
\caption{Confusion matrix}
\end{center}
\label{predict:figure:confusion_matrix}
\end{figure}

If we look at the details of the decision tree we see that the conditions use the same features as we used in the business rules. For example to predict a witness we end up testing \textbf{witness\_update\_operation}:

\begin{verbatim}
Rules used to predict sample 11683: 
decision id node 0 : (X_test[11683, limit_order_cancel_operation] (= 1) <= 572.5)
decision id node 1 : (X_test[11683, removed_ops] (= 240816) > 9999.5)
decision id node 19 : (X_test[11683, transfer_operation] (= 36) <= 10017.0)
decision id node 20 : (X_test[11683, witness_update_operation] (= 8) > 2.0)
\end{verbatim}

We can say that the predictions are very good, probably because our dataset was generated with business rules that have a clear separation of the classes using conditions. This space separation fits well the decision tree model. Also we have a lot of samples to learn from, that helps the model. The exact labels might have more nuances which may lead to more complexity so more depth in the tree. Same if we add more classes to identify. In that case we may want to use ensemble models such are bagging or random forests.