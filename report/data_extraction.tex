\chapter{Data Extraction}

In this chapter we will see how to extract the data from the BitShares blockchain using Python and which features have been selected and computed in order to build our dataset.

\section{Extraction script}

In order to extract the dataset from the BitShares blockchain a Python script has been implemented.

It is composed of a main loop that extract the data, and utility services to connect to BitShares API node and BitShares ElasticSearch instance.

\subsection{Main loop}

It first start to get the user count from the BitShares API node using the \texttt{get\_account\_count} \footnotemark endpoint, then loop to retrieve all the accounts using \texttt{get\_full\_accounts} endpoint using a list of accounts ids. Accounts ids are \texttt{1.2.x} where \texttt{x} is consecutive numbers from \textsc{100} to account count (first 100 are platform reserved accounts). The extracted information is then saved in a database (a simple CSV dump).

\footnotetext{ \url{https://dev.bitshares.works/en/master/api/blockchain_api/database.html\#get-account-count}}

The script may run for multiple hours due to the high volume of data, so to improve the performances we batch the information retrieval.. Also to avoid restart from the beginning it restarts from the last id found in the database.

This results in this Python code:

\begin{listing}[H]
\begin{codebox}{python}
nb_accounts = bitshares_ws_client.get_account_count()
batch_size = config.BATCH_SIZE

db = CsvDatabase(config.DATABASE, config.FIELDS)
first_account_id_to_load = max(100, db.last_account() + 1)
for start in range(first_account_id_to_load, nb_accounts, batch_size):
    end = min(start + batch_size, nb_accounts)
    account_ids = [ '1.2.{}'.format(i) for i in range(start, end) ]
    accounts = load_account_information(account_ids)
    db.save(accounts)
db.close()
\end{codebox}
\caption{Data extraction main loop}
\end{listing}

The implementation of \texttt{load\_account\_information} function will be described in the following sections.

\subsection{BitShares API node client}

To call the BitShares API node we use the websocket interface.
For that purpose the Python package \texttt{websocket-client} is used.

All the calls to the BitShares API node have been encapsulated in a \texttt{BitsharesWebsocketClient} class, that takes care of the websocket initialization and implement the JSON RPC over websocket protocol. The network calls are synchronous to keep the implementation simple.

The main method of the class is \texttt{def request(self, api, method\_name, params)} that will take care of the communication with the API node, and the serialization/deserialization of JSON to python objects.
This results in implementations of the retrieval methods as below:

\begin{listing}[H]
\begin{codebox}{python}
class BitsharesWebsocketClient():
    def request(self, api, method_name, params):
        # JSON RPC over websocket implementation here.
        # See Appendix for full code.
        
    def get_account_count(self):
        return self.request('database', 'get_account_count', [])

    def get_full_account(self, account_ids):
        return self.request('database', 'get_full_accounts', [account_ids, 0])
\end{codebox}
\caption{Sample of BitShares API data retrieval}
\end{listing}

The script will use the API node configured in \texttt{config.py} file, or the \texttt{WEBSOCKET\_URL} environment variable. For this project I have used the service I maintain for the community accessible at \url{wss://api.dex.trading}. 

\subsection{BitShares ElasticSearch client}

To call the BitShares ElasticSearch database a similar service class has been made. It takes care of the initialization of connection to the ElasticSearch cluster using the  \texttt{elasticsearch} official Python module, and use \texttt{elasticsearch-dsl} Python module to query the database. This last package allow to write ElasticSearch queries in a Python syntax instead of plain JSON, and takes care of the generation of the correct query.

A sample query looks like below (the exact meaning will be described in \ref{extraction:operation-count}):

\begin{listing}[H]
\begin{codebox}{python}
        s = Search(using='operations', index="bitshares-*")
        s = s.extra(size=0)
        s = s.query('bool', filter = [
            Q('terms', account_history__account__keyword=accounts_id)
        ])
        s.aggs\
            .bucket('group_by_account', 'terms', field='account_history.account.keyword',\
                    size=len(accounts_id))\
            .bucket('group_by_operation_type', 'terms', field='operation_type',\
                    size=len(operation_type_names))

        response = s.execute()
\end{codebox}
\caption{Sample BitShares ElasticSearch query using Python DSL}
\label{listing:operation-count}
\end{listing}

The script will use the ElasticSearch cluster configured in \texttt{config.py} file, or the \texttt{ELASTICSEARCH\_URL}, \texttt{ELASTICSEARCH\_USER} and \texttt{ELASTICSEARCH\_PASS} environment variable. For this project I have used the cluster maintained by oxarbitrage, a community member, reachable at \url{https://elasticsearch.bitshares-kibana.info/}.

\section{Feature selection}

In order to apply machine learning algorithms we need flat numerical data and not a graph of objects with heterogeneous data. Also the available data is so huge that a selection of features need to be made. Based of my knowledge of the system here is the initial features extracted:

\begin{itemize}
    \item \textbf{id}: id of the account to identify the line;
    \item \textbf{name}: name of the account, to help human identify the account;
    \item \textbf{auths\_owner\_count}: number of keys or accounts associated to the owner right (for multisig accounts);
    \item \textbf{auths\_active\_count}: number of keys or accounts associated to the active right (for multisig accounts)
    \item \textbf{vote\_num\_witness}: number of witnesses the account vote for;
    \item \textbf{vote\_num\_committee}: number of committee members the account vote for;
    \item \textbf{vote\_num\_votes}: number of vote given (witnesses + committee members + workers) the account vote for;
    \item \textbf{whitelisting\_accounts}: number of account white-listed;
    \item \textbf{blacklisting\_accounts}: number of accounts blacklisted;
    \item \textbf{whitelisted\_accounts}: number of accounts that has white-listed this account;
    \item \textbf{blacklisted\_accounts}: number of accounts that has blacklisted this account;
    \item \textbf{total\_ops}: total operations done since the creation of the account;
    \item \textbf{removed\_ops}: total operations canceled since the creation of the account;
    \item \textbf{lifetime\_fees\_paid}: total fees paid; 
    \item \textbf{total\_core\_in\_orders}: total BTS tokens currently in orders, this is an integer value to avoid floating numbers, it should be devided by BTS precision ($10^5$) to get the real amount;
    \item \textbf{core\_in\_balance}:total BTS tokens owned by the account;
    \item \textbf{days\_since\_last\_vote}: number of days elapsed since the last time the account voted;
    \item \textbf{nb\_assets\_owned}: number of different asset owned by this account;
    \item \textbf{operation\_type\_count*}: the count of all operations done by the account for a specific operation type, one feature for each operation type;
    \item \textbf{followers}: number of accounts who have delegated their vote to this account;
    \item \textbf{referee}: number of accounts that have been brought to the platform thanks to this account;
    \item \textbf{lifetime\_referee}:  number of accounts that have been brought to the platform thanks to this account or one of its referee;
    \item \textbf{registered}: number of accounts created by this account;
\end{itemize}

The rule used to select the features are:
\begin{itemize}
    \item Select quantity over string or categories;
    \item Avoid features too sensitive to the data extraction time (ex: pending fees to pay, open orders, ...) in order to have consistent results between extractions;
    \item Be able to retrieve the data easely and quickly to keep the extraction time small (a couple of hours);
\end{itemize}

\section{Feature extraction}

\subsection{Field mappings}

Some of the features are directly provided by the \texttt{get\_full\_accounts} API call (see Appendix \ref{appendix:api:get-full-accounts}). Here is the mapping used:

\begin{table}[h]
\begin{center}
\begin{tabular}{|l|l|}
  \hline
  Feature & JSON path \\
  \hline
    \textbf{id} & account.id \\
    \textbf{name} & account.name \\
    \textbf{vote\_num\_witness} & account.options.num\_witness \\
    \textbf{vote\_num\_committee} & account.options.num\_committee \\
    \textbf{vote\_num\_votes} & len(account.options.votes) \\
    \textbf{total\_ops} & account.statistics.total\_ops \\
    \textbf{removed\_ops} & account.statistics.removed\_ops \\
    \textbf{whitelisting\_accounts} & len(account.whitelisting\_accounts) \\
    \textbf{blacklisting\_accounts} & len(account.blacklisting\_accounts) \\
    \textbf{whitelisted\_accounts} & len(account.whitelisted\_accounts) \\
    \textbf{blacklisted\_accounts} & len(account.blacklisted\_accounts) \\
    \textbf{lifetime\_fees\_paid} & account.statistics.lifetime\_fees\_paid \\
    \textbf{total\_core\_in\_orders} & account.statistics.total\_core\_in\_orders \\
    \textbf{core\_in\_balance} & account.statistics.core\_in\_balance \\
    \hline
\end{tabular}
\end{center}
\caption{Feature extraction mapping}
\end{table}


\subsection{Computed data}

\textbf{auths\_owner\_count} and \textbf{auths\_active\_count} are computed based on \texttt{get\_full\_accounts} data, by adding all the keys, accounts and addresses authorized to sign transactions on behalf of the user. This is implemented with those formulas:

\begin{codebox}{python}
auths_owner_count = len(account.owner.key_auths)           \
                        + len(account.owner.account_auths) \
                        + len(account.owner.address_auths)

auths_active_count = len(account.active.key_auths)           \
                        + len(account.active.account_auths)  \
                        + len(account.active.address_auths)

\end{codebox}

\textbf{days\_since\_last\_vote} computes the number of days since the last vote using this formula:

\begin{codebox}{python}
    voting_date = datetime.fromisoformat(account.statistics.last_vote_time)
    elapsed = datetime.now() - voting_date
    days_since_last_vote = elapsed.days
\end{codebox}

\textbf{nb\_assets\_owned} count all the assets that have a non zero balance like this:

\begin{codebox}{python}
    nb_assets_owned = sum(1 for b in account.balances if int(b.balance) != 0)
\end{codebox}

\subsection{Operation counts} \label{extraction:operation-count}

For every account, we compute the number of operation done for each type of operation. You can see all types of operation in Appendix \ref{appendix:api:operation-types}.

This is done using ElasticSearch database, that allow to search operations by account, and group by operation type. In order to reduce the number of queries we query by batch of accounts, so we group the results first by account, then by operation type. 

The implementation of the ElasticSearch query can be seen in listing \ref{listing:operation-count}. From that we extract a dictionary of dictionary of count by operation type by account.
The result is sparse as most of the accounts do only some types of operations, we default all the not returned operation types to 0. In order to improve performances, we query only the accounts that have at least one operation using the \textbf{total\_ops} feature. See \texttt{load\_operation\_counts} in \texttt{export.py} and \texttt{get\_operation\_counts} in \texttt{bitshares\_elastic\_search\_client.py} for full implementation.

\subsection{Post processing}

Some columns needs post processing after the extraction of all the accounts data, as it's an aggregation of other accounts information.
This is the case of \textbf{followers}, \textbf{referee}, \textbf{lifetime\_referee} and \textbf{registered}, that are counts of how many other account link to another account.
Below the table of the new computed feature, the name feature that needed to be extracted for each account, and the JSON mapping to extract it.

\begin{table}[h]
\begin{center}
\begin{tabular}{|l|l|l|}
  \hline
  Computed feature name & Relation account feature &  JSON path \\
  \hline
    \textbf{followers} & \textbf{vote\_voting\_account} & account.options.voting\_account \\
    \textbf{referee} & \textbf{referrer} & account.referrer \\
    \textbf{lifetime\_referee} & \textbf{lifetime\_referrer} & account.lifetime\_referrer \\
    \textbf{registered} & \textbf{registrar} & account.registrar \\
    \hline
\end{tabular}
\end{center}
\caption{Computed features mapping}
\end{table}

To compute the values we load the dataset in a \texttt{pandas} dataframe, we group by the account feature relation we want and we count the number of accounts that have that account name. Then we add the count to the referred account using a dataframe join. We fill the account not referred with 0. Below the implementation:

\begin{listing}[H]
\begin{codebox}{python}
df = pd.read_csv(dataset_file)
df = df.set_index('id')

def add_occurrences_count(df, column_to_scan, target_column_name):
    count = df.groupby(column_to_scan)['name'].nunique()
    count = count.rename_axis('id').rename(target_column_name).to_frame()
    augmented_df = df.join(count)
    augmented_df[target_column_name] = augmented_df[target_column_name].fillna(value=0) \
                                                                       .astype(int)
    return augmented_df

df = add_occurrences_count(df, 'vote_voting_account', 'followers')
df = add_occurrences_count(df, 'referrer', 'referee')
df = add_occurrences_count(df, 'lifetime_referrer', 'lifetime_referee')
df = add_occurrences_count(df, 'registrar', 'registered')
\end{codebox}
\caption{Post processing: count link occurrences}
\end{listing}
