import csv
import os.path

class CsvDatabase():
    def __init__(self, data_file, fieldnames):
        self.data_file = data_file
        self.is_creation = not os.path.exists(data_file)
        self.csvfile = open(data_file, 'a+', newline='')
        self.writer = csv.DictWriter(self.csvfile, fieldnames=fieldnames, extrasaction='ignore')
        if self.is_creation:
            self.writer.writeheader()

    def last_account(self):
        if self.is_creation:
            return -1
        with open(self.data_file, newline='') as csvfile:
            reader = csv.reader(csvfile)
            last_account = -1
            header = True
            for row in reader:
                if header:
                    header = False
                else:
                    account_num = int(row[0].split('.')[2])
                    if last_account < account_num:
                        last_account = account_num
            return last_account

    def save(self, accounts):
        for account in accounts:
            self.writer.writerow(account)

    def close(self):
        self.csvfile.close()
