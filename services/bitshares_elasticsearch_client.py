from operation_type import get_operation_name
from elasticsearch_dsl import connections, Search, Q, A
import config
from operation_type import operation_type_names


class BitsharesElasticSearchClient():
    def __init__(self, cluster_config):
        self._create_connection('operations', cluster_config)
        self._create_connection('objects', cluster_config)

    def _create_connection(self, name, cluster_config):
        connection_config = {
            'hosts': cluster_config['hosts'],
            'timeout': 60
        }
        if 'user' in cluster_config and cluster_config['user']  \
            and 'password' in cluster_config and cluster_config['password']:
            connection_config['http_auth'] = (cluster_config['user'], cluster_config['password'])
        
        connections.create_connection(name, **connection_config)
    
    def get_operation_counts(self, accounts_id):
        if len(accounts_id) == 0:
            return {}

        s = Search(using='operations', index="bitshares-*")
        s = s.extra(size=0)
        s = s.query('bool', filter = [
            Q('terms', account_history__account__keyword=accounts_id)
        ])
        s.aggs\
            .bucket('group_by_account', 'terms', field='account_history.account.keyword', size=len(accounts_id))\
            .bucket('group_by_operation_type', 'terms', field='operation_type', size=len(operation_type_names))

        response = s.execute()

        operation_counts_by_account = {}
        for account_bucket in response.aggregations.group_by_account.buckets:
            account_id = account_bucket.key
            operation_counts = {}
            for operation_bucket in account_bucket.group_by_operation_type.buckets:
                operation_type = get_operation_name(operation_bucket.key)
                operation_counts[operation_type] = operation_bucket.doc_count
            operation_counts_by_account[account_id] = operation_counts
        return operation_counts_by_account

client = BitsharesElasticSearchClient(config.ELASTICSEARCH)
es = connections.get_connection(alias='operations')

if __name__ == "__main__":
    import pprint
    operation_counts = client.get_operation_counts(['1.2.904906', '1.2.901206', '1.2.321206'])
    pprint.pprint(operation_counts)
