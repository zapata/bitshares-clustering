import os

# BitShares Core websocket url.
# #WEBSOCKET_URL = os.environ.get('WEBSOCKET_URL', "ws://localhost:8090/ws")
WEBSOCKET_URL = os.environ.get('WEBSOCKET_URL', "wss://api.dex.trading")

# Default connection to Elastic Search.
ELASTICSEARCH = {
     'hosts': os.environ.get('ELASTICSEARCH_URL', 'https://elasticsearch.bitshares-kibana.info/').split(','),
     'user': os.environ.get('ELASTICSEARCH_USER', ''),
     'password': os.environ.get('ELASTICSEARCH_PASS', '')
}

# Database file.
DATABASE = 'data/accounts.csv'

# Max number of accounts to load. Use None to load all BitShares accounts.
NB_ACCOUNTS = os.environ.get('NB_ACCOUNTS', None)

# Load accounts by batch of this size.
BATCH_SIZE = os.environ.get('BATCH_SIZE', 500)

from operation_type import operation_type_names
FIELDS = [
    'id',
    'name',
    'auths_owner_count',
    'auths_active_count',
    'vote_num_witness',
    'vote_num_committee',
    'vote_num_votes',
    'vote_voting_account',
    'whitelisting_accounts',
    'blacklisting_accounts',
    'whitelisted_accounts',
    'blacklisted_accounts',
    'total_ops',
    'removed_ops',
    'lifetime_fees_paid',
    'total_core_in_orders',
    'core_in_balance',
#    'pending_fees',
#    'pending_vested_fees',
    'days_since_last_vote',
    'nb_assets_owned',
    'registrar',
    'referrer',
    'lifetime_referrer'
] + operation_type_names