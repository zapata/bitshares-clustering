from services.bitshares_websocket_client import client as bitshares_ws_client
from services.bitshares_elasticsearch_client import client as bitshares_es_client
from operation_type import operation_type_names
from services.csv_database import CsvDatabase
from datetime import datetime
import config
import time

def count_auths(auths):
    return len(auths['key_auths']) + len(auths['account_auths']) + len(auths['address_auths'])

def count_elapsed_days(date_iso_str):
    the_date = datetime.fromisoformat(date_iso_str)
    elapsed = datetime.now() - the_date
    return elapsed.days

def count_assets(balances):
    return sum(1 for b in balances if int(b['balance']) != 0)

def extract_account_information(full_account):
    account = {}
    account_info = full_account['account']
    account['id'] = account_info['id']
    account['name'] = account_info['name']
    # Count of keys used for mutlisig accounts
    account['auths_owner_count'] = count_auths(account_info['owner'])
    account['auths_active_count'] = count_auths(account_info['active'])
    # Voting informations
    options_info = account_info['options']
    account['vote_num_witness'] = options_info['num_witness']
    account['vote_num_committee'] = options_info['num_committee']
    account['vote_num_votes'] = len(options_info['votes'])
    account['vote_voting_account'] = options_info['voting_account']
    # White/black listing information
    account['whitelisting_accounts'] = len(account_info['whitelisting_accounts'])
    account['blacklisting_accounts'] = len(account_info['blacklisting_accounts'])
    account['whitelisted_accounts'] = len(account_info['whitelisted_accounts'])
    account['blacklisted_accounts'] = len(account_info['blacklisted_accounts'])
    # Statistics
    stats_info = full_account['statistics']
    account['total_ops'] = stats_info['total_ops']
    account['removed_ops'] = stats_info['removed_ops']
    account['lifetime_fees_paid'] = stats_info['lifetime_fees_paid']
    account['total_core_in_orders'] = stats_info['total_core_in_orders']
    account['core_in_balance'] = stats_info['core_in_balance']
    account['pending_fees'] = stats_info['pending_fees']
    account['pending_vested_fees'] = stats_info['pending_vested_fees']
    account['days_since_last_vote'] = count_elapsed_days(stats_info['last_vote_time'])
    # Assets
    account['nb_assets_owned'] = count_assets(full_account['balances'])
    # Marketing
    account['registrar'] = account_info['registrar']
    account['referrer'] = account_info['referrer']
    account['lifetime_referrer'] = account_info['lifetime_referrer']
    
    return account

def load_operation_counts(accounts):
    accounts_ids_to_load = [a['id'] for a in accounts if a['total_ops'] > 1]
    operation_counts = bitshares_es_client.get_operation_counts(accounts_ids_to_load)
    empty_operations = { operation_name : 0 for operation_name in operation_type_names }
    for a in accounts:
        a.update(empty_operations)
        if a['id'] in operation_counts:
            a.update(operation_counts[a['id']])

def load_account_information(account_ids):
    full_accounts = bitshares_ws_client.get_full_account(account_ids)
    stripped_accounts = [ extract_account_information(f[1]) for f in full_accounts ]
    load_operation_counts(stripped_accounts)

if config.NB_ACCOUNTS:
    nb_accounts = config.NB_ACCOUNTS
else:
    nb_accounts = bitshares_ws_client.get_account_count()
print("Number of accounts: {}".format(nb_accounts))
batch_size = config.BATCH_SIZE

db = CsvDatabase(config.DATABASE, config.FIELDS)
first_account_id_to_load = max(100, db.last_account() + 1)
for start in range(first_account_id_to_load, nb_accounts, batch_size):
    start_time = time.time()
    end = min(start + batch_size, nb_accounts)
    account_ids = [ '1.2.{}'.format(i) for i in range(start, end) ]
    accounts = load_account_information(account_ids)
    db.save(accounts)
    end_time = time.time()
    print('Saved {} accounts from {} to {} (in {:.2f}s)'.format(len(accounts), start, end, end_time - start_time))
db.close()
