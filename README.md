BitShares network accounts clustering and classification.
=========================================================

Environment setup
-----------------

Install virtual environment and setup:

    pip install virtualenv
    virtualenv -p python3 wrappers_env/
    source wrappers_env/bin/activate

Install dependencies:

    pip install -r requirements.txt



Remarks:
--------
   - `account.statistics.last_vote_time` is not reliable, some accounts do not vote but time is still set. Should be the last date the account has been updated.
   - How to see lifetime members?

TODO:
------
   - Add proxy weight or add non holders followers (followers with 0 bts) 
   - Add inbound / outbount number of accounts (https://www.elastic.co/guide/en/elasticsearch/guide/current/cardinality.html)

```
GET _search
{
  "size": 10,
  "query": {
    "constant_score": {
      "filter": {
        "bool": {
          "must": [
            {
              "terms": {
                "account_history.account.keyword": [
                  "1.2.904906",
                  "1.2.901206",
                  "1.2.321206"
                ]
              }
            },
            { 
              "term": {
                "operation_type": 0
              }
            }
          ]
        }
      }
    }
  }, 
  "aggs": {
    "group_by_account": {
      "terms": {
        "field": "account_history.account.keyword",
        "size": 100
      }
    }
  }
}
```