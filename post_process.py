import sys
import pandas as pd

if len(sys.argv) != 2:
    print("usage: {} file".format(sys.argv[0]))
    exit(1)

df = pd.read_csv(sys.argv[1])
df = df.set_index('id')

def add_occurrences_count(df, column_to_scan, target_column_name):
    count = df.groupby(column_to_scan)['name'].nunique()
    count = count.rename_axis('id').rename(target_column_name).to_frame()
    augmented_df = df.join(count)
    augmented_df[target_column_name] = augmented_df[target_column_name].fillna(value=0).astype(int)
    return augmented_df

df = add_occurrences_count(df, 'vote_voting_account', 'followers')
df = add_occurrences_count(df, 'referrer', 'referee')
df = add_occurrences_count(df, 'lifetime_referrer', 'lifetime_referee')
df = add_occurrences_count(df, 'registrar', 'registered')

#df = df.drop(columns=['vote_voting_account', 'referrer', 'lifetime_referrer', 'registrar'])

df.to_csv(sys.argv[1].replace('.csv', '_post_processed.csv'))