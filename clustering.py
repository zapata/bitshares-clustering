import pandas as pd
import numpy as np
from sklearn.preprocessing import FunctionTransformer, StandardScaler, PowerTransformer
from sklearn.pipeline import Pipeline
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from collections import OrderedDict
import matplotlib.pyplot as plt
import seaborn as sns
import os.path

df = pd.read_csv('data/accounts_labelized_20190424.csv')

excluded_columns = ['id', 'name', 'vote_voting_account', 'registrar', 'referrer', 'lifetime_referrer']
df = df.drop(excluded_columns, axis='columns')

def clusterize(df, standardize_mode, pca, distance_algorithm, n_clusters, only_active):
    if only_active:
        only_active_accounts = 'nb_assets_owned != 0 and lifetime_fees_paid != 0 and total_ops > 1'
        df = df.query(only_active_accounts)

    steps = []

    if standardize_mode == 'log':
        steps.append( ('log', FunctionTransformer(func=np.log1p, inverse_func=np.expm1, validate=True)) )
        steps.append( ('scale', StandardScaler()) )
    elif standardize_mode == 'std':
        steps.append( ('scale', StandardScaler()) )
    elif standardize_mode == 'power':
        steps.append(('power', PowerTransformer(method='yeo-johnson')))
    else:
        raise Exception('Invalid standardize_mode {} expecting one of {}'.format(standardize_mode, ['log', 'power']))

    if pca == 'full':
        steps.append( ('pca', PCA()) )
    elif pca == 'reduced': 
        steps.append( ('pca', PCA(n_components=30)) )
    elif pca != 'none':
        raise Exception('Invalid pca {} expecting one of {}'.format(pca, ['none', 'full', 'reduced']))

    steps.append( ('kmeans', KMeans(n_clusters=n_clusters, n_init=20, max_iter=500,n_jobs=-1, verbose=0)) )

    pipe = Pipeline(steps)
    y = pipe.fit_predict(df.drop(['is_transfer_hub', 'is_witness', 'is_trading_bot'], axis='columns').values)
    return df.assign(cluster=y)

def stats(df):
    aggs = OrderedDict([
        ('auths_owner_count', 'count'),
        ('is_trading_bot', 'sum'),
        ('is_witness', 'sum'),
        ('is_transfer_hub', 'sum')])

    stats = df.groupby('cluster').agg(aggs).rename(columns={"auths_owner_count": "count"})
    return stats

def save_plot(stats, output_file):
    fig,(ax1,tabax, ax2) = plt.subplots(1, 3, gridspec_kw={'width_ratios': [2, 1, 2]}, figsize=(10,3))
    fig.tight_layout()

    tag_matching = stats[['is_trading_bot', 'is_witness', 'is_transfer_hub']].apply(lambda x: (x / x.sum()))
    cluster_fill = stats[['is_trading_bot', 'is_witness', 'is_transfer_hub']].apply(lambda x: (x / stats['count']))

    ax1 = sns.heatmap(tag_matching, annot=True, cmap=sns.light_palette("green"), cbar=False, ax=ax1, fmt='.0%')
    ax1.set_title("Tag inclusion")

    tabax.axis("off")
    tabax.table(cellText=stats[['count']].values,
            rowLabels=stats.index,
            colLabels=['count'],
            cellLoc = 'right', rowLoc = 'center',
            loc='center')

    ax2 = sns.heatmap(cluster_fill, annot=True, cmap=sns.light_palette("green"), cbar=False, ax=ax2, fmt='.0%')
    ax2.set_title("Cluster accuracy")

    fig.savefig(output_file)
    plt.close(fig)

def experiment(df, standardize_mode, pca, distance_algorithm, n_clusters, only_active):
    print('Experimenting: standardize_mode={}, pca={}, distance_algorithm={}, n_clusters={}, only_active={}'.format(standardize_mode, pca, distance_algorithm, n_clusters, only_active))
    output_file = 'images/{}_{}_{}_{}_{}.png'.format(standardize_mode, pca, distance_algorithm, n_clusters, only_active)
    if os.path.isfile(output_file):
        print('  ... skipping as it already exists...') 
    else:
        res = clusterize(df, standardize_mode, pca, distance_algorithm, n_clusters, only_active)
        stat = stats(res)
        save_plot(stat, output_file)

for standardize_mode in ['log', 'power']:
    for pca in ['none', 'reduced', 'full']:
        #for distance_algorithm in ['euclidian', 'cosine']:
        for distance_algorithm in ['euclidian']:
            for n_clusters in [4, 8, 10]:
                #for only_active in [False, True]:
                for only_active in [True]:
                    experiment(df, standardize_mode, pca, distance_algorithm, n_clusters, only_active)

#experiment(df=df, standardize_mode='log', pca='reduced', distance_algorithm='euclidian', n_clusters=8, only_active=True, save_image=True)
#df.to_csv(path_or_buf='data/accounts_filtered_and_clustered_20190424.csv', index=False)
